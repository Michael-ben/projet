import axios from "axios";

class LoginService {
  login(user) {
    return axios
      .post(
        '/api/authenticate/login/user',
        user
      ).then(res => {
        localStorage.setItem('access_token', res.data.accessToken);
        window.location.reload();
      })
  }
}

export default new LoginService()
