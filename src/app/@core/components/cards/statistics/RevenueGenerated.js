import React from "react"
import StatisticsCard from "../../../../@vuexy/statisticsCard/StatisticsCard"
import { Package } from "react-feather"
import { revenueGeneratedSeries, revenueGenerated } from "./StatisticsData"

class RevenueGenerated extends React.Component {
  render() {
    return (
      <StatisticsCard
        icon={<Package className="success" size={22} />}
        iconBg="success"
        stat="0"
        statTitle="Document en cours de traitement"
        options={revenueGenerated}
        series={revenueGeneratedSeries}
        type="area"
      />
    )
  }
}
export default RevenueGenerated
