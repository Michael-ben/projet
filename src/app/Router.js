import React, { Suspense, lazy } from "react";
import { Router, Switch, Route } from "react-router-dom";
import { history } from "../history";
import { connect } from "react-redux";
import Spinner from "./@vuexy/spinner/Loading-spinner";
import { ContextLayout } from "./@core/utility/context/Layout";

const Auth = lazy(() =>
  import("./views/auth/Auth")
);
const Aff = lazy(() =>
  import("./views/inscription/Register")
);

const AppNew = lazy(() =>
  import("./views/application/new/New")
);

const AppList = lazy(() =>
  import("./views/application/list/List")
);
const Passe = lazy(() =>
  import("./views/auth/MotpasseOublie")
);

const ApplicationProfil = lazy(() =>
  import("./views/gestionprofil/list/List")
);
const espace = lazy(() =>
  import("./views/espace/espace")
);


const ApplicationDashboard = lazy(() =>
  import("./views/dashboard/Dashboard")
);
const ApplicationAcces = lazy(() =>
  import("./views/gestionacces/List")
);
const Applicationindividuel = lazy(() =>
  import("./views/gestionacces/modifier/modif")
);
const EditProfil = lazy(() =>
  import("./views/account-settings/AccountSettings")
);

const Corbeille = lazy(() =>
  import("./views/corbeille/Corbeille")
);
const GestionFile = lazy(() =>
  import("./views/GestionFile/Todo")
);
const profile = lazy(() =>
  import("./views/profilelist/profile")
);



// Set Layout and Component Using App Route
const RouteConfig = (
  {
    component: Component,
    fullLayout,
    landingLayout,
    withoutMenuLayout,
    permission,
    isNotSecure,
    user,
    isAuthenticated,
    ...rest
  }) => {

  return (
    <Route
      {...rest}
      render={props => {
        console.log(isAuthenticated);
        return (

          <ContextLayout.Consumer>
            {context => {
              let LayoutTag =
                fullLayout === true
                  ? context.fullLayout
                  : context.state.activeLayout === "horizontal"
                    ? context.horizontalLayout
                    : context.VerticalLayout;
              LayoutTag = withoutMenuLayout ? context.withoutMenuLayout : LayoutTag;
              return (
                <LayoutTag {...props} permission={props.user}>
                  <Suspense fallback={<Spinner />}>
                    <Component {...props} />
                  </Suspense>
                </LayoutTag>
              )
            }}
          </ContextLayout.Consumer>


        )
      }}
    />
  );
};

const mapStateToProps = state => {
  return {
    user: state.auth,
    isAuthenticated: state.auth.login.isAuthenticated
  }
};

const AppRoute = connect(mapStateToProps)(RouteConfig);

class AppRouter extends React.Component {

  render() {
    return (
      // Set the directory path if you are deploying in sub-folder
      <Router history={history}>
        <Switch>
          <AppRoute
            path="/dashboard/apps/:appId/dashboard"
            component={ApplicationDashboard}
          />


          <AppRoute
            path="/dashboard/apps/:appId/gestionprofil"
            component={ApplicationProfil}
          />
          <AppRoute
            path="/dashboard/Motdepasseoublie"
            component={Passe}
            fullLayout
          />
          <AppRoute
            path="/dashboard/Corbeille"
            component={Corbeille}
          />
          <AppRoute
            path="/dashboard/Corbeille"
            component={espace}
          />

          <AppRoute
            path="/dashboard/GestionFile"
            component={GestionFile}
          />
          <AppRoute
            path="/dashboard/profile"
            component={profile}
            fullLayout
          />
          <AppRoute
            path="/dashboard/apps/:appId/gestionacces"
            component={ApplicationAcces}
          />
          <AppRoute
            path="/dashboard/apps/individuel"
            component={Applicationindividuel}
          />
          <AppRoute
            exact
            path="/dashboard/apps/new"
            component={AppNew}
            fullLayout
          />
          <AppRoute
            path="/dashboard/apps/:appId"
            component={ApplicationDashboard}
          />
          <AppRoute
            path="/dashboard/editerprofil"
            component={EditProfil}
          />
          <AppRoute
            exact
            path="/dashboard/apps"
            component={AppList}
            fullLayout
          />

          <AppRoute
            path="/dashboard/inscription"
            component={Aff}
            fullLayout

          />
          <AppRoute
            exact
            path="/dashboard/auth"
            component={Auth}
            fullLayout
            isNotSecure
          />
          <AppRoute
            component={AppList}
            withoutMenuLayout
          />

        </Switch>
      </Router>
    )
  }
}

export default AppRouter
