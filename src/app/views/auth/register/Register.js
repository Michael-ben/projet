import React from "react"
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  FormGroup,
  Row,
  Col,
  Input,
  Form,
  Button,
  Label
} from "reactstrap"


import { loginWithJWT } from "../../../@core/redux/actions/auth/loginActions"
import { connect } from "react-redux"

class Login extends React.Component {

  state = {
    email: "demo@demo.com",
    password: "demodemo",
    remember: false
  };

  handleLogin = (e) => {
    e.preventDefault();
    this.props.loginWithJWT(this.state)
  };

  render() {
    return (
      <Card>
        <CardHeader>
          <CardTitle>Compte Invite</CardTitle>
        </CardHeader>
        <CardBody>
          <Form onSubmit={this.handleLogin}>
            <Row>
              <Col sm="12">
                <Label for="UserIcons">Code d'invitation</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="Text"
                    name="Entite"
                    id="UserIcons"
                    placeholder="Entite"
                  />

                </FormGroup>
              </Col>
              <Col sm="12">
                <Label for="IconsPassword">Mot de passe</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="password"
                    name="password"
                    id="IconsPassword"
                    placeholder="Mot de passe"
                  />

                </FormGroup>
              </Col>

              <Col sm="12">
                <FormGroup className="has-icon-left position-relative">
                  <Button.Ripple
                    color="primary"
                    type="submit"
                    className="mr-1 mb-1"
                  >
                    Se connecter
                  </Button.Ripple>
                </FormGroup>
              </Col>
            </Row>
          </Form>
        </CardBody>
      </Card>
    )
  }
}

const mapStateToProps = state => {
  return {
    values: state.auth.login
  }
};

export default connect(mapStateToProps, { loginWithJWT })(Login)
