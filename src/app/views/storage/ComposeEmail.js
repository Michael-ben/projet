import React from "react"
import { Input, Label, Card, CardHeader, CardBody, Button, FormGroup, CustomInput } from "reactstrap"
import { X } from "react-feather"
import PerfectScrollbar from "react-perfect-scrollbar"
import { EditorState } from "draft-js"
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css"
import "../../../assets/scss/plugins/extensions/editor.scss"


class ComposeEmail extends React.Component {
  state = {
    editorState: EditorState.createEmpty(),
    emailTo: "",
    emailSub: "",
    file: ""

  }
  onEditorStateChange = editorState => {
    this.setState({
      editorState
    })
  }

  handleSidebarClose = () => {
    this.props.handleComposeSidebar("close")
    this.setState({
      emailTo: "",
      emailSub: "",
      file: "",
      editorState: EditorState.createEmpty()
    })
  }

  addEmailList = () => {
    this.setState({
      file: "",
      emailSub: "",
      emailTo: "",
    })
  }


  render() {
    return (
      <Card
        className={`compose-email shadow-none ${
          this.props.currentStatus ? "open" : ""
          }`}
      >
        <CardHeader className="compose-mail-header align-items-center">
          <div className="compose-mail-title">
            <h3 className="text-bold-600 card-title">Nouveau fichier</h3>
          </div>
          <div
            className="close-compose-mail"
            onClick={() => this.props.handleComposeSidebar("close")}
          >
            <X size={20} />
          </div>
        </CardHeader>
        <PerfectScrollbar
          options={{
            wheelPropagation: false
          }}
        >
          <CardBody className="compose-mail-body p-1">
            <div className="form-label-group pt-1">
              <Input
                id="emailTo"
                placeholder="Nom du fichier"
                value={this.state.emailTo}
                onChange={e => this.setState({ emailTo: e.target.value })}
              />
              <Label for="emailTo">Nom du fichier</Label>
            </div>
            <div className="form-label-group">
              <Input
                id="subject"
                placeholder="libelle"
                value={this.state.emailSub}
                onChange={e => this.setState({ emailSub: e.target.value })}
              />
              <Label for="Subject">libelle</Label>
            </div>
            <FormGroup>
              <Label for="customFile">choisissez votre fichier</Label>
              <CustomInput
                type="file"
                id="exampleCustomFileBrowser"
                value={this.state.file}
                name="customFile"
                onChange={e => this.setState({ file: e.target.value })}
              />
            </FormGroup>

            <div className="action-btns d-flex justify-content-end mt-1">
              <Button.Ripple
                color="danger"
                className="mr-1"
                onClick={() => this.handleSidebarClose()}
              >
                Retour
              </Button.Ripple>
              <Button.Ripple
                color="primary"
                disabled={this.state.emailTo.length && this.state.emailSub.length > 0 ? false : true}
                onClick={() => this.handleSidebarClose() && this.addEmailList()}


              >
                Ajouter
              </Button.Ripple>
            </div>
          </CardBody>
        </PerfectScrollbar>
      </Card>
    )
  }
}

export default ComposeEmail
