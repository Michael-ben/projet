import React from "react"
import { FormGroup, Button, ListGroup } from "reactstrap"
import PerfectScrollbar from "react-perfect-scrollbar"
import { X, Edit } from "react-feather"
import { changeFilter } from "../../@core/redux/actions/email/index"
import { connect } from "react-redux"
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css"
import "../../../assets/scss/plugins/extensions/editor.scss"
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,

} from "reactstrap"
class EmailSidebar extends React.Component {
  state = {
    modal: false
  }
  toggleModal = () => {
    this.setState(prevState => ({
      modal: !prevState.modal
    }))
  }

  render() {
    return (
      <React.Fragment>
        <div
          className="sidebar-close-icon"
          onClick={() => this.props.mainSidebar(false)}
        >
          <X size={18} />
        </div>
        <div className="email-app-menu">
          <FormGroup className="form-group-compose text-center compose-btn">
            <Button.Ripple
              block
              className="my-2 btn-block"
              color="primary"
              onClick={() => {
                this.props.handleComposeSidebar("open")
                this.props.mainSidebar(false)
              }}
            >
              <Edit size={14} />
              <span className="align-middle ml-50">Ajouter un fichier</span>
            </Button.Ripple>
          </FormGroup>
          <PerfectScrollbar
            className="sidebar-menu-list"
            options={{
              wheelPropagation: false
            }}
          >
            <ListGroup className="list-group-messages font-medium-1">

              <Button
                onClick={this.toggleModal}
              >
                creer un dossier
              </Button>

              <Modal
                isOpen={this.state.modal}
                toggle={this.toggleModal}
                className="modal-dialog-centered"
              >
                <ModalHeader toggle={this.toggleModal} className="bg-primary">
                  Creer un dossier
                </ModalHeader>
                <ModalBody>
                  <Input
                    type="text"
                    id="floatingInput"
                    placeholder="entrer le nom du dossier"
                  />
                </ModalBody>
                <ModalFooter>
                  <Button outline color="primary" onClick={this.toggleModal}>
                    Annuler
                 </Button>{" "}
                  <Button color="primary" onClick={this.toggleModal}>
                    Creer
                 </Button>{" "}
                </ModalFooter>
              </Modal>

            </ListGroup>
          </PerfectScrollbar>
        </div>
      </React.Fragment>
    )
  }
}

export default connect(null, { changeFilter })(EmailSidebar)
