import React from "react"
import { FormGroup, Input} from "reactstrap"
import { Menu, Search } from "react-feather"
import PerfectScrollbar from "react-perfect-scrollbar"
import { connect } from "react-redux"
import {
  getTodos,
  completeTask,
  starTask,
  importantTask,
  trashTask,
  searchTask
} from "../../@core/redux/actions/todo/index"

class TodoList extends React.Component {
  static getDerivedStateFromProps(props, state) {
    if (props.app.todo.routeParam !== state.currentLocation) {
      return {
        todos: props.app.todo.todos
      }
    }
    // Return null if the state hasn't changed
    return null
  }
  state = {
    todos: [],
    handleUpdateTask: null,
    currentLocation: this.props.routerProps.location.pathname,
    value: ""
  }
  async componentDidMount() {
    await this.props.getTodos(this.props.routerProps.match.params)
    this.setState({
      todos: this.props.app.todo.todos,
      handleUpdateTask: this.props.handleUpdateTask
    })
  }

  handleOnChange = e => {
    this.setState({ value: e.target.value })
    this.props.searchTask(e.target.value)
  }



  render() {

    return (
      <div className="content-right">
        <div className="todo-app-area">
          <div className="todo-app-list-wrapper">
            <div className="todo-app-list">
              <div className="app-fixed-search">
                <div
                  className="sidebar-toggle d-inline-block d-lg-none"
                  onClick={() => this.props.mainSidebar(true)}
                >
                  <Menu size={24} />
                </div>
                <FormGroup className="position-relative has-icon-left m-0 d-inline-block d-lg-block">
                  <Input
                    type="text"
                    placeholder="Search..."
                    onChange={e => this.handleOnChange(e)}

                  />
                  <div className="form-control-position">
                    <Search size={15} />
                  </div>
                </FormGroup>
              </div>
              <div>

              </div>

              <PerfectScrollbar
                className="todo-task-list"
                options={{
                  wheelPropagation: false
                }}
              >
              </PerfectScrollbar>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    app: state.todoApp
  }
}
export default connect(mapStateToProps, {
  getTodos,
  completeTask,
  starTask,
  importantTask,
  trashTask,
  searchTask
})(TodoList)
