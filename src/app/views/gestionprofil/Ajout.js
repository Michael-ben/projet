import React from "react"
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter, FormGroup,Label,Input
} from "reactstrap"

class ModalForm extends React.Component {

  state = {
    modal: false
  }

  toggleModal = () => {
    this.setState(prevState => ({
      modal: !prevState.modal
    }))
  }

  render() {
    return(
    <div>
      <Button color="primary" onClick={this.toggleModal}>
        Ajouter
      </Button>
      <Modal
        isOpen={this.state.modal}
        toggle={this.toggleModal}
        className="modal-dialog-centered"
      >
        <ModalHeader toggle={this.toggleModal}>
          Creer profil
        </ModalHeader>
        <ModalBody>
          <FormGroup>
            <Label for="email">Email:</Label>
            <Input
              type="email"
              id="email"
              placeholder="Entrer E-mail"
            />
          </FormGroup>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.toggleModal}>
            Ajouter
          </Button>{" "}
        </ModalFooter>
      </Modal>
    </div>

    )
  }
}
export default ModalForm
