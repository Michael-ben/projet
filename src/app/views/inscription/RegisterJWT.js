import React from "react"
import { Form, FormGroup, Input, Label, Row, Col } from "reactstrap"
import { connect } from "react-redux"
import { signupWithJWT } from "../../@core/redux/actions/auth/registerActions"


class RegisterJWT extends React.Component {
  state = {
    email: "",
    password: "",
    name: "",
    confirmPass: ""
  }

  handleRegister = e => {
    e.preventDefault()
    this.props.signupWithJWT(
      this.state.email,
      this.state.password,
      this.state.name
    )
  }

  render() {
    return (

      <Form className="mt-0">
        <Row>
          <Col md="6" sm="12">
            <FormGroup >
              <Label for="nameVertical">Nom</Label>
              <Input
                type="text"
                name="Nom"
                id="nameVertical"
                placeholder="Nom"
              />

            </FormGroup>
          </Col>
          <Col md="6" sm="12">
            <FormGroup >
              <Label for="lastNameMulti">Prenoms</Label>
              <Input
                type="text"
                name="Prenoms"
                id="lastNameMulti"
                placeholder="Prenoms"
              />

            </FormGroup>
          </Col>

          <Col md="6" sm="12">
            <Label for="CountryMulti">E-mail</Label>
            <FormGroup >
              <Input
                type="email"
                name="email"
                id="CountryMulti"
                placeholder="Email"
              />
            </FormGroup>

          </Col>
          <Col md="6" sm="12">
            <Label for="CompanyMulti">Raison sociale</Label>
            <FormGroup >
              <Input
                type="text"
                name="Raison sociale"
                id="CompanyMulti"
                placeholder="Raison sociale"
              />

            </FormGroup>
          </Col>


        </Row>




        <div className="d-flex justify-content-between">
          <a href='/dashboard/auth' id="buyButton" className="btn btn-primary"> S'inscrire</a>
        </div>
      </Form>

    )
  }
}
const mapStateToProps = state => {
  return {
    values: state.auth.register
  }
}
export default connect(mapStateToProps, { signupWithJWT })(RegisterJWT)
