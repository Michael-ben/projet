import React, { Component } from 'react';
import {
  Row, Col
} from 'reactstrap';
import * as Icon from "react-feather"

class Footer extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <React.Fragment>
        <footer>
          <div className="justify-content-center bg-dark">
            <Row className="align-items-start ml-5">
              <Col>
                <h2>A propos de nous</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Corrupti atque eius quae itaque voluptates animi! Ratione accusantium
                temporibus in! Cumque dolorem sed totam ipsa sapiente reiciendis laborum
                harum dicta fugit.
                  </p>
              </Col>
              <Col>
                <h2>Nous suivre</h2>
                <Icon.Facebook size={42} className="mr-4 fonticon-wrap" />
                <label className="fonticon-classname">Facebook</label>
                <Icon.Instagram size={42} className="mr-4 fonticon-wrap" />
                <label className="fonticon-classname">Facebook</label>
                <Icon.Twitter size={42} className="mr-4 fonticon-wrap" />
                <label className="fonticon-classname">Facebook</label>
              </Col>
              <Col>
                <h2>Contact</h2>
                <Icon.Mail size={42} className="mr-4 fonticon-wrap" />
                <label className="fonticon-classname">info@ akiltechnologies.com</label>
              </Col>
            </Row>

          </div>
        </footer>

      </React.Fragment>
    );
  }
}

export default Footer;
