import React, { Component } from 'react';
import { Row, Card, Col, Button, FormGroup, Label, Input } from 'reactstrap';


// Import Generic Components

class Contact extends Component {


  constructor(props) {
    super(props);
    this.state = {};
    this.scrollNavigation = this.scrollNavigation.bind(this);

  }

  componentDidMount() {
    document.body.classList = "";
    window.addEventListener("scroll", this.scrollNavigation, true);
  }

  // Make sure to remove the DOM listener when the component is unmounted.
  componentWillUnmount() {
    window.removeEventListener("scroll", this.scrollNavigation, true);
  }

  scrollNavigation = () => {
    const doc = document.documentElement;
    const top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    if (top > 80) {
      document.getElementById('topnav').classList.add('nav-sticky');
    } else {
      document.getElementById('topnav').classList.remove('nav-sticky');
    }
  };


  render() {
    return (
      <div className="bg-half-170">
        <Card className="faq-bg">
          <Row className="m-0 mt-5 justify-content-center">
            <Col lg="4" md="6" sm="9" >
              <h1>
                -CONTACT
                </h1>
              <card>
                <FormGroup>
                  <Input
                    type="text"
                    name="name"
                    id="nameVertical"
                    placeholder="Nom"
                  />
                </FormGroup>
                <FormGroup>
                  <Input
                    type="email"
                    name="Email"
                    id="nameVertical"
                    placeholder="Email"
                  />
                </FormGroup>
                <FormGroup>
                  <Input
                    type="text"
                    name="Objet"
                    id="nameVertical"
                    placeholder="Objet"
                  />
                </FormGroup>
                <FormGroup className="form-label-group mt-2">
                  <Label for="nameVertical">Nom</Label>
                  <Input
                    type="textarea"
                    name="text"
                    id="exampleText"
                    rows="3"
                    placeholder="Message"
                  />
                  <Label>Message</Label>
                </FormGroup>
                <Col sm="12">
                  <FormGroup>
                    <Button type="reset" className="mr-1 mb-1" color="success" onClick={() => this.handleAlert("successAlert", false)} >Valider</Button>
                    <Button
                      outline
                      color="warning"
                      type="reset"
                      className="mb-1"
                    >
                      Annuler
                      </Button>

                  </FormGroup>
                </Col>
              </card>
            </Col>
            <Col lg="7" md="6" sm="8">
              <iframe _ngcontent-c2="" allowfullscreen="" frameborder="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.202932324548!2d-3.9894700850723543!3d5.386009236833227!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfc19355fcdd76d3%3A0x122291c8c750a062!2sStrataige+Business+Expert+Consult!5e0!3m2!1sfr!2sci!4v1554997215808!5m2!1sfr!2sci" title="Akil" width="850" height="480"></iframe>
            </Col>
          </Row>
        </Card>
      </div>
    );
  }
}

export default Contact;
