import React from "react"
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  FormGroup,
  Row,
  Col,
  Input,
  Form,
  Button,
  Label
} from "reactstrap"
import SweetAlert from 'react-bootstrap-sweetalert';

class TableContent extends React.Component {
  state = {
    successAlert: false
  }

  handleAlert = (state, value) => {
    this.setState({ [state]: value })
  }
  render() {
    return (
      <Card>
        <CardHeader>
          <CardTitle>Posez votre question</CardTitle>
        </CardHeader>
        <CardBody>
          <Form>
            <Row>
              <Col sm="12">
                <FormGroup>
                  <Label for="nameVertical">Nom</Label>
                  <Input
                    type="text"
                    name="name"
                    id="nameVertical"
                    placeholder="Nom"
                  />
                </FormGroup>
              </Col>
              <Col sm="12">
                <FormGroup className="form-label-group mt-2">
                  <Label for="nameVertical">Nom</Label>
                  <Input
                    type="textarea"
                    name="text"
                    id="exampleText"
                    rows="3"
                    placeholder="Votre question"
                  />
                  <Label>Votre Question</Label>
                </FormGroup>
              </Col>
              <Col sm="12">
                <FormGroup>
                  <Button type="reset" className="mr-1 mb-1" color="success" onClick={() => this.handleAlert("successAlert", true)} >Valider</Button>
                  <SweetAlert success title="Success"
                    show={this.state.successAlert}
                    onConfirm={() => this.handleAlert("successAlert", false)}
                  >
                    <p className="sweet-alert-text">Votre question nous a ete envoye</p>
                  </SweetAlert>
                  <Button
                    outline
                    color="warning"
                    type="reset"
                    className="mb-1"
                  >
                    Annuler
                  </Button>
                </FormGroup>
              </Col>

            </Row>
          </Form>
        </CardBody>
      </Card>
    )
  }
}
export default TableContent
